from neptune_python_utils.gremlin_utils import GremlinUtils
from neptune_python_utils.endpoints import Endpoints

endpoints = Endpoints(
    neptune_endpoint='database-2.cluster-cte1sjaxdhke.us-east-1.neptune.amazonaws.com',
    region_name='us-east-1d',
    proxy_dns=None,
    proxy_port=8182,
    remove_host_header=False)

GremlinUtils.init_statics(globals())

gremlin_utils = GremlinUtils(endpoints)

conn = None

try:
    conn = gremlin_utils.remote_connection(ssl=False)
    g = gremlin_utils.traversal_source(connection=conn)
    g.addV('person').property(id, '2').property('name', 'vadas').property('age', 27).iterate()
    g.addV('software').property(id, '3').property('name', 'lop').property('lang', 'java').iterate()
    g.addV('person').property(id, '4').property('name', 'josh').property('age', 32).iterate()
    g.addV('software').property(id, '5').property('name', 'ripple').property('lang', 'java').iterate()
    g.addV('person').property(id, '6').property('name', 'peter').property('age', 35)
    print(g.V().limit(10).valueMap().toList())
except Exception as e:
    print(e)
finally:
    if conn:
        conn.close()
